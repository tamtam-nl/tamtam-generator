'use strict';
var util = require('util');
var mv = require('mv');
var spawn = require('child_process').spawn;
var path = require('path');
var chalk = require('chalk');
var yeoman = require('yeoman-generator');

var HtmlGenerator = module.exports = function HtmlGenerator(args, options, config) {

	yeoman.generators.Base.apply(this, arguments);

    this.on('end', function () {

		if (this.options['skip-install']) {
            return;
		 }

	  	this.installDependencies({
	    	skipMessage: this.options['skip-install-message'],
	    	skipInstall: this.options['skip-install'],
			callback: function() {
				// Emit a new event - dependencies installed
                if (this.includeBootstrap) {
                    this.emit('removeBowerFromBowerJson');
                } else{
                    this.emit('npmDependenciesInstalled');
                }
			}.bind(this)
	  	});

        // Now you can bind to the dependencies installed event
        this.on('removeBowerFromBowerJson', function() {
            // Move bootstrap (if bootstrap is choosen) to libs after getting it from bower
            var self = this;
            mv(process.cwd() + '/bower_components/' +  this.bootstrapVersion, process.cwd() + '/libs/bootstrap', { mkdirp : true }, function(err){});
            console.log(chalk.bold('Moved ' + self.bootstrapVersion + ' to /libs/'));
            var bowerUninstall = this.spawnCommand('bower', ['uninstall', this.bootstrapVersion, '--save']);
            bowerUninstall.on('close', function (code) {
                console.log(chalk.bold('Removed ' + self.bootstrapVersion + ' from bower.json'));
                self.emit('npmDependenciesInstalled');
            });
        });

        // Now you can bind to the dependencies installed event
        this.on('npmDependenciesInstalled', function() {
            // All dependencies installed
            console.log(chalk.green.bold('All dependencies installed'));
            console.log(chalk.yellow.bold("Now run: "), chalk.inverse.bold('grunt server'));
            this.spawnCommand('grunt', ['server']);
        });
	});

	this.pkg = JSON.parse(this.readFileAsString(path.join(__dirname, 'package.json')));
};

util.inherits(HtmlGenerator, yeoman.generators.Base);

HtmlGenerator.prototype.askFor = function askFor() {

	var cb = this.async();

	var prompts = [
		{
			type: 'confirm',
			name: 'includeCMS',
			message: 'Are you going to use a CMS (like Umbraco)',
			default: true
		},
		{
			type: 'list',
			name: 'projectLanguage',
			message: 'What language do your users speak?',
			choices : [
				{
					value:'nl',
					name:'Dutch'
				},
				{
					value:'en',
					name:'English'
				},
				{
					value:'es',
					name:'Spanish'
				},
				{
					value:'de',
					name:'German'
				},
				{
					value:'pt',
					name:'Portugese'
				},
				{
					value:'fr',
					name:'French'
				},
				{
					value:'it',
					name:'Italian'
				}
			]
		},
		{
			type: 'input',
			name: 'projectName',
			message: 'What is the name of your project',
			validate: function(input) {
				if (input) {
					return true;
				} else {
					return "You need to specify a project name";
				}
			}
		},
		{
			type: 'checkbox',
			name: 'chooseLibraries',
			choices : [
				{
					value: 'jquery',
					name: 'jQuery',
					checked: false
				},
				{
					value: 'modernizr',
					name: 'Modernizr',
					checked: false
				},
				{
					value: 'bootstrap',
					name: 'Bootstrap',
					checked: false
				}
			],
			message: 'Which libraries would you like to use?'
		},
		{
			type: 'confirm',
			name: 'includeGruntRev',
			message: 'Would you like to use static file asset revisioning through content hashing? (example: c338b5de.scripts.js)',
			default: false
		}
	];

	this.prompt(prompts, function (props) {

		var libraries = props.chooseLibraries;

		function hasFeature(feat,array) {
			return array.indexOf(feat) !== -1;
		}

		function capitaliseFirstLetter(string) {
			return string.charAt(0).toLowerCase() + string.slice(1);
		}

		function trimString(string) {
			var replacedString =  string.replace(/\s/g, "").replace("-", "").replace("_", "").replace(".", "").replace(",", "");
			return capitaliseFirstLetter(replacedString);
		}

		this.includeCMS             = props.includeCMS;
		this.projectLanguage        = props.projectLanguage;
        this.projectName            = props.projectName;
        this.dashedProjectName      = props.projectName.replace(/ /gi, "-");
		this.computedProjectName    = trimString(props.projectName);
		this.includeGruntRev        = props.includeGruntRev;
		this.chooseLibraries        = props.chooseLibraries;

		this.includejQuery      = hasFeature('jquery',libraries);
		this.includeBootstrap   = hasFeature('bootstrap',libraries);
		this.includeModernizr   = hasFeature('modernizr',libraries);
        this.bootstrapVersion   = 'bootstrap-sass-official';

		// Set bootstrap to PreProcessor Version
		if (this.includeBootstrap){
			var index = this.chooseLibraries.indexOf('bootstrap');
			if (index > -1) {
				this.chooseLibraries.splice(index,1);
				this.chooseLibraries.push(this.bootstrapVersion);
			}
		}

		if (this.includeCMS) {
			this.Frontend = "Frontend/";
			this.Backend = "Backend/"
		} else {
			this.Frontend = "";
			this.Backend = ""
		}

		cb();
	}.bind(this));
};

HtmlGenerator.prototype.general = function general() {

	// Creating Directories
	console.log(chalk.magenta.bold('Creating base directories and templates'));

	if (this.includeCMS) {
		this.mkdir("Frontend");
		this.mkdir("Backend");
	}

	this.mkdir(this.Frontend + 'html');
	this.mkdir(this.Frontend + 'html/includes');
	this.mkdir(this.Frontend + 'images');
    this.mkdir(this.Frontend + 'fonts');
	this.mkdir(this.Frontend + 'js');

	// Generation templates
	this.template('_index.html', this.Frontend + 'html/index.html');
	this.copy('_styleguide.html', this.Frontend + 'html/styleguide.html');
	this.copy('_gitignore', this.Frontend + '.gitignore');

	this.copy('includes/_header.html',this.Frontend + 'html/includes/header.html');
	this.copy('includes/_footer.html',this.Frontend + 'html/includes/footer.html');
	this.copy('includes/_scripts.html',this.Frontend + 'html/includes/scripts.html');
	this.copy('includes/_navigation.html',this.Frontend + 'html/includes/navigation.html');

	this.copy('js/config.js',this.Frontend + 'js/config.js');

};

HtmlGenerator.prototype.bower = function bower() {
 	this.copy('_bowerrc', this.Frontend + '.bowerrc');
	this.copy('_bower.json', this.Frontend + 'bower.json');
};

HtmlGenerator.prototype.gruntfile = function gruntfile() {
	this.template('Gruntfile.js',this.Frontend + 'Gruntfile.js');
};

HtmlGenerator.prototype.packageJSON = function packageJSON() {
  this.template('_package.json', this.Frontend + 'package.json');
};


HtmlGenerator.prototype.preprocessor = function preprocessor() {

	this.log.ok(chalk.green.bold('Directories and templates are created'));
	this.mkdir(this.Frontend + 'sass');

	var sassDir = (this.Frontend + 'sass');

	console.log(chalk.magenta.bold('Creating SASS directories and templates'));

	this.copy('sass/main.scss', sassDir + '/main.scss');
    this.copy('sass/base/_general.scss', sassDir + '/base/_general.scss');
    this.copy('sass/base/_utils.scss', sassDir + '/base/_utils.scss');
    this.copy('sass/base/_responsive.scss', sassDir + '/base/_responsive.scss');
    this.copy('sass/components/_module.scss', sassDir + '/components/_module.scss');
};

HtmlGenerator.prototype.projectfiles = function projectfiles() {
	this.log.ok(chalk.green.bold('SASS directories and templates are created'));
	this.copy('editorconfig', this.Frontend + '.editorconfig');
    this.copy('jshintrc', this.Frontend + '.jshintrc');
    this.copy('readme.txt', this.Frontend + 'readme.txt');
};

HtmlGenerator.prototype.install = function install() {
	// Dependencies array
	var dependencies = [];

	// Has Bootrap? Push to Dependencies
	if (this.includeBootstrap) {
		dependencies.push(this.bootstrapVersion);
	}
	// Has Modernizr? Push to Dependencies
	if (this.includeModernizr) {
		dependencies.push('modernizr');
	}
	// Has jQuery? Push to Dependencies
	if (this.includejQuery || this.includeBootstrap) {
		dependencies.push('jquery');
	}
	if (this.includeCMS) {
		var bowerdir = this.Frontend;
		process.chdir(bowerdir);
	}
	this.bowerInstall(dependencies, {
		save: true
	});
};

