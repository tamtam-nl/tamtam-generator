'use strict';

var util = require('util');
var spawn = require('child_process').spawn;
var mv = require('mv');
var path = require('path');
var chalk = require('chalk');
var yeoman = require('yeoman-generator');

var AngularGenerator = module.exports = function AngularGenerator(args, options, config) {

	yeoman.generators.Base.apply(this, arguments);

	this.on('end', function () {

		if (this.options['skip-install']) {
		   	return;
		 }

        this.installDependencies({
            skipMessage: this.options['skip-install-message'],
            skipInstall: this.options['skip-install'],
            callback: function() {
                // Emit a new event - dependencies installed
                if (this.includeBootstrap) {
                    this.emit('removeBowerFromBowerJson');
                } else{
                    this.emit('npmDependenciesInstalled');
                }
            }.bind(this)
        });

        // Now you can bind to the dependencies installed event
        this.on('removeBowerFromBowerJson', function() {
            // Move bootstrap (if bootstrap is choosen) to libs after getting it from bower
            var self = this;
            mv(process.cwd() + '/bower_components/' +  this.bootstrapVersion, process.cwd() + '/libs/bootstrap', { mkdirp : true }, function(err){});
            console.log(chalk.bold('Moved ' + self.bootstrapVersion + ' to /libs/'));
            var bowerUninstall = this.spawnCommand('bower', ['uninstall', this.bootstrapVersion, '--save']);
            bowerUninstall.on('close', function (code) {
                console.log(chalk.bold('Removed ' + self.bootstrapVersion + ' from bower.json'));
                self.emit('npmDependenciesInstalled');
            });
        });

        // Now you can bind to the dependencies installed event
        this.on('npmDependenciesInstalled', function() {
            // All dependencies installed
            console.log(chalk.green.bold('All dependencies installed'));
            console.log(chalk.yellow.bold("Now run: "), chalk.inverse.bold('grunt server'));
            this.spawnCommand('grunt', ['server']);
        });
    });

	this.pkg = JSON.parse(this.readFileAsString(path.join(__dirname, 'package.json')));
};

util.inherits(AngularGenerator, yeoman.generators.Base);

AngularGenerator.prototype.askFor = function askFor() {

	var cb = this.async();

	var prompts = [
		{
			type: 'list',
			name: 'projectLanguage',
			message: 'What language do your users speak?',
			choices : [
				{
					value:'nl',
					name:'Dutch'
				},
				{
					value:'en',
					name:'English'
				},
				{
					value:'es',
					name:'Spanish'
				},
				{
					value:'de',
					name:'German'
				},
				{
					value:'pt',
					name:'Portugese'
				},
				{
					value:'fr',
					name:'French'
				},
				{
					value:'it',
					name:'Italian'
				}
			]
		},
		{
			type: 'input',
			name: 'projectName',
			message: 'What is the name of your project?',
			validate: function(input) {
				if (input) {
					return true;
				} else {
					return "You need to specify a project name";
				}
			}
		},
		{
			type: 'checkbox',
			name: 'angularModules',
			choices : [
				{
					value:'ngRoute',
					name:'angular-route'
				},
                {
                    value:'ui.router',
                    name:'angular-ui-router'
                },
				{
					value:'ngResource',
					name:'angular-resource'
				},
				{
					value:'ngAnimate',
					name:'angular-animate'
				},
				{
					value:'ngSanitize',
					name:'angular-sanitize'
				}
			],
			message: 'Which AngularJS Modules would you like to use?'
		},
		{
			type: 'checkbox',
			name: 'chooseLibraries',
			choices : [
				{
					value: 'jquery',
					name: 'jQuery',
					checked: false
				},
				{
					value: 'modernizr',
					name: 'Modernizr',
					checked: false
				},
				{
					value: 'bootstrap',
					name: 'Bootstrap',
					checked: false
				}
			],
			message: 'What other libraries would you like to use?'
		},
		{
            type: 'confirm',
            name: 'includeGruntRev',
            message: 'Would you like to use static file asset revisioning through content hashing? (example: c338b5de.scripts.js)',
            default: false
		},
        {
            type: 'confirm',
            name: 'includeTesting',
            message: 'Would you like to work with unit / e2e testing (karma, mocha, protractor)',
            default: false
        }
	];

	this.prompt(prompts, function (props) {

		var libraries = props.chooseLibraries;

		function hasFeature(feat,array) {
			return array.indexOf(feat) !== -1;
		}

		function lowercaseFirstLetter(string) {
			return string.charAt(0).toLowerCase() + string.slice(1);
		}

		function trimString(string) {
			var replacedString =  string.replace(/\s/g, "").replace("-", "").replace("_", "").replace(".", "").replace(",", "");
			return replacedString;
		}

        // Set names
		this.projectLanguage        = props.projectLanguage;
		this.projectName            = props.projectName;
        this.dashedProjectName      = props.projectName.replace(/ /gi, "-");
		this.computedProjectName    = trimString(props.projectName);
        this.fileProjectName        = lowercaseFirstLetter(this.computedProjectName);
        this.includeGruntRev        = props.includeGruntRev;
		this.chooseLibraries        = props.chooseLibraries;
		this.angularModules         = props.angularModules;
        this.includeTesting         = props.includeTesting;
		this.includejQuery          = hasFeature('jquery',libraries);
		this.includeBootstrap       = hasFeature('bootstrap',libraries);
		this.includeModernizr       = hasFeature('modernizr',libraries);
		this.bootstrapVersion       = 'bootstrap-sass-official';

		// Set bootstrap to PreProcessor Version
		if (this.includeBootstrap){
			var index = this.chooseLibraries.indexOf('bootstrap');
			if (index > -1) {
				this.chooseLibraries.splice(index,1);
				this.chooseLibraries.push(this.bootstrapVersion);
			}
		}

		// Add Angular Modules if chosen
		this.includeAngularRoute    = hasFeature('ngRoute', this.angularModules);
		this.includeAngularResource = hasFeature('ngResource', this.angularModules);
		this.includeAngularAnimate  = hasFeature('ngAnimate', this.angularModules);
		this.includeAngularSanitize = hasFeature('ngSanitize', this.angularModules);
        this.includeAngularUIRouter = hasFeature('ui.router', this.angularModules);

		cb();
	}.bind(this));
};

AngularGenerator.prototype.general = function general() {

	// Creating Directories
	console.log(chalk.magenta.bold('Creating base directories and templates'));

	this.mkdir('html');
	this.mkdir('html/includes');
	this.mkdir('html/views');
	this.mkdir('images');
    this.mkdir('fonts');
	this.mkdir('js');

	// Generation templates
	this.copy('_index.html', 'html/index.html');
	this.copy('_styleguide.html', 'html/styleguide.html');
	this.copy('_gitignore', '.gitignore');

	this.copy('includes/_header.html','html/includes/header.html');
	this.copy('includes/_footer.html','html/includes/footer.html');
	this.copy('includes/_scripts.html','html/includes/scripts.html');
	this.copy('includes/_navigation.html','html/includes/navigation.html');

	this.copy('js/config.js','js/config.js');
};

AngularGenerator.prototype.bower = function bower() {
 	this.copy('_bowerrc', '.bowerrc');
	this.copy('_bower.json', 'bower.json');
};

AngularGenerator.prototype.gruntfile = function gruntfile() {
	this.template('Gruntfile.js');
};

AngularGenerator.prototype.packageJSON = function packageJSON() {
  this.template('_package.json', 'package.json');
};

AngularGenerator.prototype.angular = function angular() {

	console.log(chalk.magenta.bold('Creating AngularJS directories and templates'));

	this.mkdir('js/angular');
	this.mkdir('js/angular/modules');
	this.mkdir('js/angular/controllers');
	this.mkdir('js/angular/services');
	this.mkdir('js/angular/directives');
	this.mkdir('js/angular/filters');
	this.mkdir('js/angular/interceptors');
	this.mkdir('js/angular/transformers');
	this.copy('js/angular/modules/app.js', 'js/angular/modules/' + this.fileProjectName + '.js');
	this.copy('js/angular/modules/app-run.js', 'js/angular/modules/' + this.fileProjectName + '-run.js');
	this.copy('js/angular/modules/app-config.js', 'js/angular/modules/' + this.fileProjectName + '-config.js');
	this.copy('js/angular/services/example.js', 'js/angular/services/example.js');
	this.copy('js/angular/controllers/example.js', 'js/angular/controllers/example.js');
    this.copy('js/angular/directives/example.js', 'js/angular/directives/example.js');
    this.copy('js/angular/filters/example.js', 'js/angular/filters/example.js');
	this.copy('js/angular/interceptors/example.js', 'js/angular/interceptors/example.js');
	this.copy('js/angular/transformers/example.js', 'js/angular/transformers/example.js');
};

AngularGenerator.prototype.testing = function testing() {
        if (this.includeTesting) {

            console.log(chalk.magenta.bold('Creating test directories and templates'));

            this.mkdir('test');
            this.copy('test/karma-shared.conf.js', 'test/karma-shared.conf.js');
            this.copy('test/karma-unit.conf.js', 'test/karma-unit.conf.js');
            this.copy('test/mocha.conf.js', 'test/mocha.conf.js');
            this.copy('test/protractor.conf.js', 'test/protractor.conf.js');
            this.mkdir('test/e2e');
            this.copy('test/e2e/testExample.js', 'test/e2e/testExample.js');
            this.mkdir('test/lib');
            this.copy('test/lib/chai-should.js', 'test/lib/chai-should.js');
            this.copy('test/lib/chai-expect.js', 'test/lib/chai-expect.js');
            this.copy('test/lib/chai-assert.js', 'test/lib/chai-assert.js');
            this.mkdir('test/unit');
            this.copy('test/unit/testExample.js', 'test/unit/testExample.js');
        }
};

AngularGenerator.prototype.preprocessor = function preprocessor() {

    console.log(chalk.magenta.bold('Creating SASS directories and templates'));

    this.mkdir('sass');
    this.copy('sass/main.scss', 'sass/main.scss');
    this.copy('sass/base/_general.scss', 'sass/base/_general.scss');
    this.copy('sass/base/_utils.scss', 'sass/base/_utils.scss');
    this.copy('sass/base/_responsive.scss', 'sass/base/_responsive.scss');
    this.copy('sass/components/_module.scss', 'sass/components/_module.scss');

};

AngularGenerator.prototype.projectfiles = function projectfiles() {
	this.copy('editorconfig', '.editorconfig');
    this.copy('jshintrc', '.jshintrc');
    this.copy('readme.txt', 'readme.txt');

    this.log.ok(chalk.green.bold('All directories and templates are created'));

};

AngularGenerator.prototype.install = function install() {

    // Dependencies array
	var dependencies = [];

	// Has Bootrap? Push to Dependencies
	if (this.includeBootstrap) {
		dependencies.push(this.bootstrapVersion);
	}

	// Angular
	dependencies.push('angular');
    dependencies.push('angular-i18n');

	// Check AngularJS Modules
	if (this.includeAngularRoute) {
		dependencies.push('angular-route');
	}
	if (this.includeAngularResource) {
		dependencies.push('angular-resource');
	}
	if (this.includeAngularAnimate) {
		dependencies.push('angular-animate');
	}
	if (this.includeAngularSanitize) {
		dependencies.push('angular-sanitize');
	}
    if (this.includeAngularUIRouter) {
        dependencies.push('angular-ui-router');
    }

    // Unit / Integration test dependencies
    if (this.includeTesting) {
        dependencies.push('angular-mocks');
    }
	// Has Modernizr? Push to Dependencies
	if (this.includeModernizr) {
		dependencies.push('modernizr');
	}
	// Has jQuery?
	if (this.includejQuery || this.includeBootstrap) {
		dependencies.push('jquery');
	}

	this.bowerInstall(dependencies, {
		save: true
	});
};

