'use strict';
var path = require('path');
var util = require('util');
var chalk = require('chalk');
var yeoman = require('yeoman-generator');
var request = require('request-json');

var TamtamGenerator = module.exports = function TamtamGenerator(args, options, config) {

	yeoman.generators.Base.apply(this, arguments);

};

util.inherits(TamtamGenerator, yeoman.generators.NamedBase);


TamtamGenerator.prototype.askFor = function askFor() {
	var cb = this.async();
	var pkg = JSON.parse(this.readFileAsString(path.join(__dirname, '../app/package.json')));

		this.tamtam = chalk.red('\n ______   __     __   __       __   ______   __     __   __' +
		'\n|__ __|  /  \\   |  \\ /  |     / /  |_   _|  /  \\   |  \\ /  |' +
		'\n  | |   / /\\ \\  |   /   |    / /     | |   / /\\ \\  |   /   |' +
		'\n  | |  / ____ \\ | |\\_/| |   / /      | |  / ____ \\ | |\\_/| |' +
		'\n  |_| /_/    \\_\\|_|   |_|  /_/       |_| /_/    \\_\\|_|   |_|');

	// have Yeoman greet the user.
	console.log(this.tamtam);
	console.log(chalk.white.bgRed.bold('\n Tam Tam Generator (' + pkg.version + ')' ));
	console.log(chalk.blue.bold('\nLets startup your project\n'));

	var prompts = [
		{
			type: 'list',
			name: 'chooseMethod',
			choices : [
				{
					value: 'existingProject',
					name: 'I want to work on a existing project'
				},
				{
					value: 'newProject',
					name: 'I want to start a new project'
				}
			],
			message: 'What do you want to do?'
		},
		{
			type: 'list',
			name: 'choosePlatform',
			message: 'Which platform would you like to use',
			default: 'html',
			choices : [
				{
					value: 'html',
					name: 'HTML (with or without CMS)',
					checked: false
				},
				{
					value: 'angular',
					name: 'AngularJS',
					checked: false
				}
			],
			when: function(input) {
				return input.chooseMethod.indexOf('newProject') !== -1
			}
		},
	];

	this.prompt(prompts, function (props) {

		this.project = props.chooseMethod;
		this.platform = props.choosePlatform;

		cb();

	}.bind(this));
};




TamtamGenerator.prototype.main = function app() {
	// Here: we'are calling the nested generator (via 'invoke' with options)
	if (this.project === 'existingProject') {
		this.invoke( "tamtam:git", { options: {nested: true} } );
	}
	if (this.platform === 'html') {
		this.invoke( "tamtam:html", {options: {nested: true} } );
	}
    else if (this.platform === 'angular') {
        this.invoke( "tamtam:angular", {options: {nested: true} } );
    }
};
